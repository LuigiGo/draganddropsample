package com.raywenderlich.cheesefinder.dragdrop;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.raywenderlich.cheesefinder.R;

public class DragAndDropActivity extends Activity
        implements View.OnClickListener, View.OnTouchListener {

    int windowwidth;
    int windowheight;

    ImageView ima1, ima2, image;
    Button btnOk;
    RelativeLayout relativeLayout;
    android.widget.RelativeLayout.LayoutParams layoutParams;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dragdrop);

        /**Initializing views*/
        setupViews();
    }

    private void setupViews() {
        /**Layouts*/
        relativeLayout = (RelativeLayout) findViewById(R.id.relative);

        /**ImageViews*/
        ima1 = (ImageView) findViewById(R.id.imgPictureTaken);
        ima2 = (ImageView) findViewById(R.id.imgDraggableImage);
        image = (ImageView) findViewById(R.id.imgResult);

        /**Buttons*/
        btnOk = (Button) findViewById(R.id.btnOk);

        /**Listeners*/
        btnOk.setOnClickListener(this);
        ima2.setOnTouchListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnOk:
                /**Converting two bitmaps into single bitmap*/
                Bitmap bitmap1 = ((BitmapDrawable) ima1.getDrawable()).getBitmap();
                Bitmap bitmap2 = ((BitmapDrawable) ima2.getDrawable()).getBitmap();

                windowwidth = relativeLayout.getMeasuredWidth();
                windowheight = relativeLayout.getMeasuredHeight();

                Bitmap bitmap3 = overlay(bitmap1, bitmap2);
                image.setImageBitmap(bitmap3);
                break;
        }
    }

    /**
     * For merging two bitmaps into single bitmap
     */
    private Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {

        /**rectF will resize the generated bitmap in canvas*/
        RectF rectF = new RectF(ima2.getX() / 2, ima2.getY() / 2, ima2.getX() / 2 + 25, ima2.getY() / 2 + 25);

        /**Merging two bitmaps*/
        Bitmap bmOverlay = Bitmap.createBitmap(windowwidth, windowheight, bmp1.getConfig());
        Canvas canvas1 = new Canvas(bmOverlay);
        canvas1.drawBitmap(bmp1, new Matrix(), null);
        canvas1.drawBitmap(bmp2, null, rectF, null);
        return bmOverlay;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (view.getId()) {
            case R.id.imgDraggableImage:
                layoutParams = (RelativeLayout.LayoutParams) ima2.getLayoutParams();
                switch (event.getActionMasked()) {

                    case MotionEvent.ACTION_DOWN:
                        /**Start dragging*/
                        break;

                    case MotionEvent.ACTION_MOVE:
                        /**Currently dragging the view*/
                        int x_cord = (int) event.getRawX() / 2;
                        int y_cord = (int) event.getRawY() / 2;

                        if (x_cord > windowwidth) {
                            x_cord = windowwidth;
                        }
                        if (y_cord > windowheight) {
                            y_cord = windowheight;
                        }

                        ima2.setX(x_cord);
                        ima2.setY(y_cord);
                        break;

                    default:
                        /**Default*/
                        break;
                }
                break;
        }
        return true;
    }
}
